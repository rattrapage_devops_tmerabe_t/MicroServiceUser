<?php
Doo::loadModel('base/AnnoncesBase');
Doo::loadModel('base/UserAnnonceRelationBase');
Doo::loadModel('base/DocumentConsultationPermitBase');

class Annonces extends AnnoncesBase {

	public function setDetails($annonces, $idUser = NULL) {

		foreach ($annonces as $annonce) {
			// PARSING FLOAT
			$annonce->loyer = floatval($annonce->loyer);
			
			// PHOTOS
			$res = $annonce->relatePhotos(array());
			$res ? $annonce->Photos = $res[0]->Photos : $annonce->Photos = [];
			
			// PROPRIO
			$u = new Users();
			$res = $u->find(array(
					'where' => 'idUser = ' . $annonce->idProprio
			));
			$res ? $annonce->Possesseur = $res[0] : $annonce->Possesseur = [];
			
			// NB FAVORIS
			$uar = new UserAnnonceRelation();
			$res = $uar->count(array(
					'where' => 'idAnnonce = ' . $annonce->idAnnonce
			));
			$res ? $annonce->NBFavoris = $res[0] : $annonce->NBFavoris = [];
			
			// IS IN FAVORIS && document consultation permit
			if ($idUser) {
				$ua = new UserAnnonceRelation();
				$opt = array(
						'where' => 'idAnnonce = ? and idUser = ?',
						'param' => array(
								$annonce->idAnnonce,
								$idUser
						)
				);
				empty($ua->find($opt)) ? $annonce->isInFavoris = 0 : $annonce->isInFavoris = 1;
				$dcp = new DocumentConsultationPermit();
				$opt = array(
						'where' => 'idAnnonce = ? and idUser = ?',
						'param' => array(
								$annonce->idAnnonce,
								$idUser
						)
				);
				empty($dcp->find($opt)) ? $annonce->isDocumentConsultationPermit = 0 : $annonce->isDocumentConsultationPermit = 1;
			}
		}
		return $annonces;
	}

	public function find($opt = NULL, $idUser = NULL) {

		$annonces = parent::_find("Annonces", $opt);
		if (empty($annonces)) {
			return null;
		}
		return $this->setDetails($annonces, $idUser);
	}

	public function relate($model = NULL, $models = NULL, $opt = NULL, $idUser = NULL) {

		$annonces = parent::_relate($model, $models, $opt);
		if (empty($annonces)) {
			return null;
		}
		return $this->setDetails($annonces, $idUser);
	}

}