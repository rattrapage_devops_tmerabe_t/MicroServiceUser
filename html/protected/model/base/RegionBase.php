<?php
Doo::loadCore('db/DooModel');

class RegionBase extends DooModel{

    /**
     * @var int Max length is 11.
     */
    public $region_id;

    /**
     * @var int Max length is 5.
     */
    public $region_code;

    /**
     * @var char Max length is 30.
     */
    public $region_nom;

    public $_table = 'region';
    public $_primarykey = 'region_id';
    public $_fields = array('region_id','region_code','region_nom');

    public function getVRules() {
        return array(
                'region_id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'optional' ),
                ),

                'region_code' => array(
                        array( 'integer' ),
                        array( 'maxlength', 5 ),
                        array( 'notnull' ),
                ),

                'region_nom' => array(
                        array( 'maxlength', 30 ),
                        array( 'notnull' ),
                )
            );
    }

}