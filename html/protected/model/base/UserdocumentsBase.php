<?php
Doo::loadCore('db/DooModel');

class UserDocumentsBase extends DooModel{

    /**
     * @var int Max length is 11.
     */
    public $idDocument;

    /**
     * @var int Max length is 11.
     */
    public $idUser;

    /**
     * @var varchar Max length is 500.
     */
    public $link;

    /**
     * @var char Max length is 250.
     */
    public $nomDoc;

    /**
     * @var date
     */
    public $date;

    /**
     * @var char Max length is 2.
     */
    public $public;

    public $_table = 'user_documents';
    public $_primarykey = 'idDocument';
    public $_fields = array('idDocument','idUser','link','nomDoc','date','public');

    public function getVRules() {
        return array(
                'idDocument' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'optional' ),
                ),

                'idUser' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'notnull' ),
                ),

                'link' => array(
                        array( 'maxlength', 500 ),
                        array( 'notnull' ),
                ),

                'nomDoc' => array(
                        array( 'maxlength', 250 ),
                        array( 'notnull' ),
                ),

                'date' => array(
                        array( 'date' ),
                        array( 'notnull' ),
                ),

                'public' => array(
                        array( 'maxlength', 2 ),
                        array( 'notnull' ),
                )
            );
    }

}