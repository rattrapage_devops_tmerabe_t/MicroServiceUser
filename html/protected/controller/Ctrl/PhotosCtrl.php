<?php

Doo::loadModel('Users');
Doo::loadModel('Annonces');
Doo::loadModel('Photos');
Doo::loadModel('PhotoAnnonce');
Doo::loadController('BDDController');

class PhotosCtrl extends BDDController {

	public function getAllPhotos() {

		$p = new Photos();
		$opt = array(
				'select' => '*',
				'match' => 'false',
				'AsArray' => 'true'
		);
		$annonces = $p->relateAnnonces($opt);
		return $this->renderJSON(json_encode($annonces));
	}

	public function choosePrincipale() {

		$data = json_decode(file_get_contents("php://input"));
		$param = $data->lien;
		$idAnnonce = $data->idAnnonce;
		$annonce = Annonces::_find("Annonces", array(
				'where' => 'idAnnonce=?',
				'param' => array(
						$idAnnonce
				)
		));
		$annonce = array_pop($annonce);
		$annonce->photoPrincipale = $param;
		$annonce->update();
		return $this->renderJSON(json_encode("Photo modifiée"));
	}

	public function uploadPictureAnnonce() {

		$param = $this->params['idAnnonce'];
		$date = date('l_jS_F_Y_H\hi\ms\s');
		$uploadFolder = "upload/annonce/annonce_" . $param . "_" . $date;
		$data = file_get_contents("php://input");
		$data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));
		$result = file_put_contents($uploadFolder, $data);
		if (! $result) {
			return $this->renderJSON(json_encode($result), self::ERR_SYS);
		}
		
		$photo = new Photos();
		$photo->lien = $uploadFolder;
		$idPhoto = $photo->insert();
		
		$pa = new PhotoAnnonce();
		$pa->idPhoto = $idPhoto;
		$pa->idAnnonce = $this->params['idAnnonce'];
		$pa->insert();
		
		return $this->renderJSON(json_encode("Photo modifiée"));
	}

	public function deleteUserPicture() {

		$param = $this->params['idUser'];
		$u = Users::_find("Users", array(
				'where' => 'idUser=?',
				'param' => array(
						$param
				)
		));
		if ($u[0]->lienPhoto != "icon.png") {
			$to_delete = $u[0]->lienPhoto;
			$u[0]->lienPhoto = "icon.png";
			$u[0]->update();
			array_map('unlink', glob($to_delete));
		}
		return $this->renderJSON(json_encode("Photo supprimée"));
	}

	public function deletePictureAnnonce() {

		$param = $this->params['idPhoto'];
		$p = Photos::_find("Photos", array(
				'select' => '*',
				'where' => 'idPhoto=?',
				'param' => array(
						$this->params['idPhoto']
				)
		));
		$to_delete = $p[0]->lien;
		Photos::_delete("Photos", array(
				'where' => 'idPhoto=?',
				'param' => array(
						$this->params['idPhoto']
				)
		));
		array_map('unlink', glob($to_delete));
		return $this->renderJSON(json_encode("Photo supprimée"));
	}

	public function uploadPicture() {

		$param = $this->params['idUser'];
		array_map('unlink', glob("upload/user/user_$param*"));
		$date = date('l_jS_F_Y_H\hi\ms\s');
		$uploadFolder = "upload/user/user_" . $param . "_" . $date;
		$data = file_get_contents("php://input");
		$data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));
		$result = file_put_contents($uploadFolder, $data);
		if (! $result) {
			return $this->renderJSON(json_encode($result), self::ERR_SYS);
		} else {
			$u = Users::_find("Users", array(
					'where' => 'idUser=?',
					'param' => array(
							$this->params['idUser']
					)
			));
			$u[0]->lienPhoto = $uploadFolder;
			$u[0]->update();
			return $this->renderJSON(json_encode("Photo modifiee"));
		}
	}

}