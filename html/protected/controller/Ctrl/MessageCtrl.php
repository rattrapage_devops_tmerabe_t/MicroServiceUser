<?php

Doo::loadModel('Message');
Doo::loadModel('Users');
Doo::loadController('BDDController');
Doo::loadModel('Annonces');

class MessageCtrl extends BDDController {

	public function getUsersDiscuss() {

		$idUser = $this->params['idUser'];
		$message = new Message();
		$opt = array(
				'where' => 'idFrom = ? OR idTo = ?',
				'param' => array(
						$idUser,
						$idUser
				),
				'desc' => 'sendDate'
		);
		return $this->renderJSON(json_encode($message->find($opt)));
	}

	public function getAllDiscuss() {

		$idUser = $this->params['idUser'];
		// result: 5, exepted: 3
		$message = new Message();
		$opt = array(
				'where' => '(idFrom = ? OR idTo = ?)',
				'param' => array(
						$this->params['idUser'],
						$this->params['idUser']
				),
				'desc' => 'sendDate'
		);
		$messages = $message->relateUsers($opt);
		
		if (! $messages) {
			return $this->renderJSON(json_encode(null));
		}
		
		$result = [];
		$alreadyInResult = [];
		foreach ($messages as $message) {
			if (false == $this->searchForDuplicates($alreadyInResult, $message->idFrom, $message->idTo)) {
				if ($message->idFrom != $idUser) {
					$alreadyInResult[] = $message->idFrom;
				} else if ($message->idTo != $idUser) {
					$alreadyInResult[] = $message->idTo;
					$message->Users = Users::_find("Users", array(
							"where" => "idUser = " . $message->idTo,
							"limit" => 1
					));
					$message->Annonces = Annonces::_find("Annonces", array(
							"where" => "idAnnonce = " . $message->idAnnonce,
							"limit" => 1
					));
				}
				$result[] = $message;
			}
		}
		return $this->renderJSON(json_encode($result));
	}

	public function searchForDuplicates($alreadyInResult, $idFrom, $idTo) {

		foreach ($alreadyInResult as $id) {
			if ($id == $idFrom || $id == $idTo) {
				return true;
			}
		}
		return false;
	}

	public function saveMessage() {

		$data = json_decode(file_get_contents("php://input"));
		$message = new Message($data);
		return $this->renderJSON(json_encode($message->insert()));
	}

	public function getAllMesages() {

		$message = new Message();
		$opt = array(
				'where' => '(idFrom = ? AND idTo = ?) OR (idFrom = ? AND idTo = ?)',
				'param' => array(
						$this->params['idFirstUser'],
						$this->params['idOtherUser'],
						$this->params['idOtherUser'],
						$this->params['idFirstUser']
				),
				'asc' => 'sendDate'
		);
		return $this->renderJSON(json_encode($message->find($opt)));
	}

	public function getAllMessagesUnread() {

		$message = new Message();
		$opt = array(
				'where' => '(idTo = ?) AND (unread = 1)',
				'param' => array(
						$this->params['idUser']
				)
		);
		return $this->renderJSON(json_encode($message->find($opt)));
	}

	public function setOlderMessagesToRead() {

		$message = new Message();
		$opt = array(
				'where' => '(idFrom = ? AND idTo = ?) AND unread = 1 AND sendDate < ?',
				'param' => array(
						$this->params['idOtherUser'],
						$this->params['idFirstUser'],
						date("Y-m-d H:i:s")
				)
		);
		$data = array(
				'unread' => '0',
				'readDate' => date("Y-m-d H:i:s")
		);
		return $this->renderJSON(json_encode($message->updateAttributes($data, $opt)));
	}

}