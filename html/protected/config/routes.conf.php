<?php

$route['get']['/genModel'] = array(
		'BDDController',
		'genModel'
);
$route['get']['/genBDD'] = array(
		'BDDController',
		'genBDD'
);

/**
 * USERS
 */
// GET
$route['get']['/user/:idUser'] = array(
		'Ctrl/UserCtrl',
		'getOneById'
);
$route['get']['/user'] = array(
		'Ctrl/UserCtrl',
		'getAll'
);

$route['get']['/userMock'] = array(
		'Ctrl/UserCtrl',
		'userMock'
);

$route['get']['/userMock/:idUser'] = array(
		'Ctrl/UserCtrl',
		'getUserMock'
);


// PUT
//met à jour un user
$route['put']['/user/:id'] = array(
		'Ctrl/UserCtrl',
		'UpdateOne'
);

// POST
//Ajoute un user
$route['post']['/user'] = array(
		'Ctrl/UserCtrl',
		'saveOne'
);
?>